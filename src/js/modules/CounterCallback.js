


class CounterCallback {
    constructor() {
        this.callbacks = {};
    }

    AddCallback(type, callback) {
        this.callbacks[type] = callback;
    }

    GetCallback(type) {
        if ( this.callbacks[type] ) {
            return this.callbacks[type];
        } else {
            return (e) => {};
        }
    }
}

module.exports = new CounterCallback();