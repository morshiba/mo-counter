


class TickerSpeed {

    constructor(format) {
        this.format = format;
    }

    Get() {
        var response = 1000;
        switch(this.format[this.format.length-1]) {
            case "s":
                response = 1000;
                break;
            case "m":
                response = 60 * 1000;
                break;
            case "h":
                response = 60 * 60 * 1000;
                break;
            case "d":
                response = 24 * 60 * 60 * 1000;
                break;
            case "w":
                response = 7 * 24 * 60 * 60 * 1000;
                break;
            case "m":
                response = 4 * 7 * 24 * 60 * 60 * 1000;
                break;
            case "y":
                response = 12 * 4 * 7 * 24 * 60 * 60 * 1000;
                break;
            case "ms":
                response = 100;
                break;
            default:
                break;
        }
        return response;
    }
}

module.exports = TickerSpeed;