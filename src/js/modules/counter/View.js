
import Uid from '../tools/Uid.js';
import CounterNumberView from './CounterNumberView.js';

class View {

    constructor(options) {
        this.options = options;
        this.id = Uid.Id;
        this.className = 'mocounter';
        this.views = {};
        this.visibilityManager = null;
    }

    Build(data) {
        this.target = document.createElement('div');
        this.target.id = this.id;
        this.target.className = this.className;
        var format = this.options.format;
        var first = true;

        if ( this.options.Prefix !== "" ) {
            let prefix = document.createElement('div');
            prefix.className ="moview moprefix";
            prefix.innerHTML = this.options.Prefix;
            this.target.appendChild(prefix);
        }
        for ( var i=0; i<format.length; i++ ) {
            var current = format[i];
            var value = 0;
            if ( !first && data[current].rest && data[current].rest > -1 ) {
                value = data[current].rest;
            } else {
                value = data[current].full;
            }
            
            var view = new CounterNumberView(this.options, current, 'mo', current);
            if ( this.options.RemoveEmpty === 1 && value === 0 && first && !this.options.IsTimePart(current) ) {
                // do nothing until the first filled value
            } else {
                if ( !this.options.IsTimePart(current) ) {
                    first = false;
                }
                // first = false;
                this.views[current] = view;
                this.target.appendChild(view.View);
            }
        }
        return this.target;
    }

    Update(data) {
        let format = this.options.Format;
        let first = true;
        for ( var i=0; i<format.length; i++ ) {
            var current = format[i];
            var value = 0;
            var time = data[current].rest;
            if ( time && time > -1 ) {
                value = time;
            }
            if ( this.views[current] && this.views[current] !== null ) {
                if ( this.options.RemoveEmpty === 1 && data[current].full <= 0 && first && !this.options.IsTimePart(current)) {
                    this.views[current].remove();
                    this.views[current] = null;
                    delete(this.views[current]);
                } else {
                    if ( first === true ) {
                        // if ( data[current].full > data[current].rest ) {
                            value = data[current].full;
                        // }
                    }
                    first = false;
                    if ( this.views[current] && this.views[current] !== null ) {
                        this.views[current].Update(value, this.options.Speed);
                    }
                }
            }
        }
    }

    get Id() {
        return this.id;
    }
}

module.exports = View;