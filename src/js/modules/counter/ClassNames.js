

class ClassNames {
    constructor() {
        return {
            y: "year",
            mo: "month",
            w: "week",
            d: "day",
            h: "hour",
            m: "minute",
            s: "seconds",
            ms: "miliseconds"
        };
    }
}

module.exports = new ClassNames();