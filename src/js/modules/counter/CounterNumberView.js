

// import JSAnimatedNumber from '../tools/JSAnimatedNumber';
import ClassNames from './ClassNames';
import NumberView from '../tools/NumberView'; 

class CounterNumberView extends NumberView {

    constructor(options, prefix, cssClass, current) {
        super(options, prefix, cssClass);
        this.value = "";
        this.current = current;
        this.divider = null;
        this.Init();
    }

    Init() {
        this.view = document.createElement(this.tagType);
        this.view.className = 'moview moview-to';

        this.view.appendChild(this.BuildNumbers());

        this.divider = document.createElement(this.tagType );
        this.divider.className = "mo-d modivider modivider-" + ClassNames[this.current];
        this.view.appendChild(this.divider);
        this.divider.innerHTML = this.options.GetDivider(0, this.current);
    }

    Update(value, speed) {
        this.number.Update(value, speed);
        this.divider.innerHTML = this.options.GetDivider(value, this.current);
    }

}

module.exports = CounterNumberView;