
import TimeParser from '../tools/TimeParser';
import View from './View';
import TickerSpeed from './TickerSpeed';
import EventManager from '../tools/Eventmanager';
import AttributReader from '../tools/AttributReader';
import Options from '../tools/Options';
import VisibilityManager from '../tools/VisibilityManager';

class SingleCounter {

    constructor(target) {
        this.attributeReader = new AttributReader();
        this.parser = new TimeParser();
        this.options = new Options();
        this.view = null;

        this.ticker = null;
        this.target = target;
        this.Init();
    }

    Init() {
        let time;
        this.options = this.attributeReader.Parse(this.target, this.options);
        this.options.Type = "counter";
        this.options = this.parser.Direction(this.options);

        if ( this.options.Direction == "down" ) {
            time = Math.floor(this.options.TargetTime -  new Date());
        } else if (this.options.Direction == "up") {
            time = Math.floor(new Date() - this.options.TargetTime);
        }
        this.options.AnimationDirection = this.target.classList.contains('mo-up') ? "up" : this.target.classList.contains('mo-down') ? "down" : "";
        this.options.Tween = this.target.classList.contains('mo-tween') ? 1 : 0;
        this.parser.Parse(time);
        // console.log("this.parser.Data", this.parser.Data);
        // console.log("this.options.UpdateSize", this.options.UpdateSize);
        this.options.UpdateSize(this.parser.Data);
        this.view = new View(this.options);
        this.Append();
        var speed = new TickerSpeed(this.options.Format).Get();
        this.Tick();
        this.ticker = setInterval(this.Tick.bind(this), speed);
        if ( this.options.Debug === 1 ) {
            setTimeout(function() {
                clearInterval(this.ticker);
                this.ticker = null;
            }.bind(this), this.options.KillAfter);
        }
        this.visibilityManager = new VisibilityManager();
        this.visibilityManager.Register(this.OnVisibilityChange.bind(this));
    }


    OnVisibilityChange(status) {
        if ( status === false ) {
            clearInterval(this.ticker);
            this.ticker = null;
        } else {
            var speed = new TickerSpeed(this.options.Format).Get();
            this.ticker = setInterval(this.Tick.bind(this), speed);
        }
    }

    Append() {
        this.target.appendChild(this.view.Build(this.parser.Data));
        this.view.Update(this.parser.Data);
    }

    Tick() {
        let time,wait = false;
        if ( this.options.StartTime !== null && this.options.StartTime >= new Date() ) {
            time = 0;
            wait = true;
        } else {
            let starttime = new Date();
            if ( this.options.Direction === "down" ) {
                // calcdirection = "down";
                time = Math.floor(this.options.TargetTime -  starttime);
            } else if ( this.options.Direction === "up") {
                // calcdirection = "up";
               time = Math.floor(starttime - this.options.TargetTime);
            }
        }
        if ( time <= 0 && !wait) {
            EventManager.Throw("complete", {id: this.view.Id, options: this.options, target: this.target});
            clearInterval(this.ticker);
            this.ticker = null;
        }
        this.parser.Parse(time);
        this.view.Update(this.parser.Data);
    }

}

module.exports = SingleCounter;