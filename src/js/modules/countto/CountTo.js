

import AttributReader from '../tools/AttributReader';
import Options from '../tools/Options';
import View from './View';
import Ticker from './Ticker';

class CountTo {

    constructor(target) {
        this.attributeReader = new AttributReader();
        this.options = new Options();
        this.view = null;
        this.ticker = null;
        this.target = target;
        this.ticker = null;
        this.Init();
    }

    Init() {
        this.options = this.attributeReader.Parse(this.target, this.options);
        this.options.Type = this.options.Type === "" ? "goto" : this.options.Type;
        this.options.AnimationDirection = this.target.classList.contains('mo-up') ? "up" : this.target.classList.contains('mo-down') ? "down" : "";
        this.options.Tween = this.target.classList.contains('mo-tween') ? 1 : 0;
        this.ticker = new Ticker(this.options);
        this.view = new View(this.options, this.ticker);

        this.Append();
    }


    Append() {
        // this.target.appendChild(this.view.Build());
        this.view.Build(this.target);
        this.options.Current = this.ticker.Current();
    }

}

module.exports = CountTo;