


class Ticker {

    constructor(options) {
        this.options = options;
        this.positions = [];
        this.position = 0;
        this.maxTicks = 0;
        this.tickTime = 1;
        this.Init();
    }

    Init() {
        let i, j, tick, parts, offset, max;
        let time = this.options.Time;
        let diff = this.options.Stop - this.options.Start;

        
        this.tickTime = time / diff;
        if (time > diff) {
            this.tickTime = time / diff;
        } else {
            this.tickTime = diff / time;
        }
        max = 10;
        if (this.tickTime < this.options.Speed) {
            this.tickTime = this.options.Speed;
        }
        
        if (this.options.Type == "goto") {
            switch (this.options.Ease) {
                default:
                    let percent = 100 / time * this.tickTime;
                    offset = diff / 100 * percent;

                    tick = this.options.Start;
                    this.positions.push(this.GetDezimalTick(tick));

                    while (tick + offset < this.options.Stop) {
                        tick += offset;
                        this.positions.push(this.GetDezimalTick(tick));
                    }
                    this.positions.push(String(this.options.Stop));
                    break;
            }
        } else if (this.options.Type == "slots") {
            tick = this.options.Stop;
            switch (this.options.Ease) {
                default:
                    max = 10;
                    if (this.options.Speed && this.options.Speed > 0 ) {
                        this.tickTime = this.options.Speed;
                    }
                    this.positions = [];
                    let stop = this.options.Stop;
                    if ( String(stop).indexOf('.') > -1 ) {
                        parts = String(stop).split('.');
                    } else {
                        parts = [stop];
                        parts.push("0");
                    }
                    if ( parts[1].length < this.options.Dezimal ) {
                        while(parts[1].length < this.options.Dezimal ) {
                            parts[1] = parts[1] + "0";
                        }
                    } else if ( parts[1].length >= this.options.Dezimal ) {
                        parts[1] = parts[1].substr(0, this.options.Dezimal);
                    }
                    parts = parts.join('').split('');
                    let tmptick = [];
                    let initvalue = [];
                    let partLength = parts.length;

                    for (j = 0; j < partLength; j++) {
                        tmptick[j] = "0";
                        initvalue[j] = Number(parts[j]);
                    }
                    this.positions.push(tmptick);
                    let offset = 1;
                    let startNext = 0;
                    let activePart = 0;
                    this.maxTicks = time / this.tickTime;

                    for (i = 0; i < this.maxTicks - 1; i++) {
                        let singletick = [];
                        for (j = 0; j < partLength; j++) {
                            if (activePart >= j) {
                                singletick[j] = Math.round(Math.random() * (9));
                                while(singletick[j] == tmptick[j] ) {
                                    singletick[j] = Math.round(Math.random() * (9));
                                } 
                            } else {
                                singletick[j] = 0;
                            }
                            if ( startNext == i ) {
                                startNext += offset;
                                activePart++;
                            }
                        }
                        this.positions.push(singletick);
                        tmptick = singletick;
                    }
                    this.positions.push(initvalue);
                    break;
            }
        }
    }

    GetDezimalTick(tick) {
        if (this.options.Dezimal > 0) {
            if ( String(tick).indexOf('.') > -1 ) {
                let parts = String(tick).split('.');
                if ( parts[1].length > this.options.Dezimal ) {
                    parts[1] = parts[1].substr(0, this.options.Dezimal);
                } else {
                    switch (this.options.Dezimal) {
                        case 1:
                            if ( parts[1].length < 1 ) {
                                parts[1] = "0";
                            }
                            break;
                        default:
                            let singles = parts[1].split("");
                            while ( singles.length < this.options.length ) {
                                singles.push("0");
                            }
                            parts[1] = singles.join(""); 
                            break;
                    }
                }
                tick = parts.join('.');
            }
        } else {
            tick = Math.floor(Math.round(tick));
        }
        return tick;
    }

    get TickTime() {
        return this.tickTime;
    }

    get Percent() {
        return Math.floor(Math.round(100 / (this.positions.length-1) * this.position));
    }

    Reset() {
        this.position = 0;
        if ( this.options.Type == "slots") {
            this.Init();
        }
    }

    Next() {
        this.position++;
        this.position = this.position > this.positions.length - 1 ? this.positions.length - 1 : this.position;
        return this.positions[this.position];
    }

    Current() {
        let response = this.positions[this.position];
        return response;
    }

    Last() {
        this.position = this.positions.length - 1;
        let response = this.positions[this.position];
        return response;
    }

    Prev() {
        this.position--;
        this.position = this.position < 0 ? 0 : this.position;
        return this.positions[this.position];
    }

    Finished() {
        // console.log("finished", this.position, this.positions.length);
        if (this.position >= this.positions.length-1) {
            return true;
        }
        return false;
    }

    GetSpeedByTime() {
        let result;
        let time = this.options.Time;
        let diff = Math.abs(this.options.Start - this.options.Stop);

        switch (this.options.Ease) {
            default:
                result = time / diff * this.options.Current;
                break;
        }
        return result;
    }

    GetPositionInPercent() {
        let diff = Math.abs(this.options.Start - this.options.Stop);
        return 100 / diff * this.options.Current;
    }
}

module.exports = Ticker;