

import Uid from '../tools/Uid.js';
import CounterNumberView from './CounterNumberView.js';
import EventManager from '../tools/Eventmanager';
import ScrollListener from '../tools/ScrollListener';
import VisibilityManager from '../tools/VisibilityManager';

class View {

    constructor(options, ticker) {
        this.options = options;
        this.ticker = ticker;
        this.id = Uid.Id;
        this.className = 'mocounter';
        this.views = {};
        this.target = null;
        this.started = false;
        this.finished = false;
        this.scrollListener = null;
        this.tickTimeout = null;
        this.visibilityManager = null;
        this.timeoutStartTime = null;
        this.timeoutStopTime = null;
    }

    Build(counterTarget) {
        this.target = document.createElement('div');
        this.target.id = this.id;
        this.target.className = this.className;
        this.AddSpecial(this.options.Before, 'before');

        this.AddNumberView();

        this.AddSpecial(this.options.After, 'after');
        counterTarget.appendChild(this.target);
        this.scrollListener = new ScrollListener(this.target);

        // console.log("this.options.Debug", this.options.Debug);
        if (this.options.startinview == 1 && this.options.Debug < 1) {
            // start on Scroll
            this.options.SetCurrent(this.ticker.Current());
            this.Update(this.options.Current, this.ticker.TickTime);
            this.scrollListener.Init(this.OnScrollUpdate.bind(this));
        } else if (this.options.Debug < 1) {
            // start directly
            this.options.SetCurrent(this.ticker.Current());
            this.Start();
        } else {
            // set end value but do not start
            this.options.SetCurrent(this.ticker.Last());
            this.Update(this.options.Current, this.ticker.TickTime);
        }
        this.visibilityManager = new VisibilityManager();
        this.visibilityManager.Register(this.OnVisibilityChange.bind(this));
        // return this.target;
    }

    OnVisibilityChange(status) {
        // console.log("countto:: View:: OnVisibilityChange: status", status);
        let date;
        let scope = this;
        if ( status === false ) {
            if ( this.options.Timeout > 0 && this.started === true && this.finished !== true ) {
                date = new Date();
                this.timeoutStopTime = date.getTime();
            }
            this.StopTicker();
        } else {
            if ( this.started === true && this.finished === false ) {
                if ( this.options.Timeout > 0 ) {
                    let diff = this.timeoutStopTime - this.timeoutStartTime;
                    if ( diff < this.options.Timeout ) {
                        date = new Date();
                        diff = this.options.Timeout  - diff;
                        this.timeoutStartTime = date.getTime();
                        this.timeoutStartTime -= diff;
                        this.tickTimeout = setTimeout(function () {
                            EventManager.Throw("start", { id: scope.id, options: scope.options, target: scope.target, percent: scope.ticker.Percent });
                            scope.tickTimeout = setTimeout(scope.Tick.bind(scope), 1, false);
                        }.bind(this), diff);
                    } else {
                        EventManager.Throw("restart", { id: this.id, options: this.options, target: this.target, percent: this.ticker.Percent });
                        this.tickTimeout = setTimeout(this.Tick.bind(this), this.ticker.TickTime, false);
                    }
                } else {
                    EventManager.Throw("restart", { id: this.id, options: this.options, target: this.target, percent: this.ticker.Percent });
                        this.tickTimeout = setTimeout(this.Tick.bind(this), this.ticker.TickTime, false);
                }
            }
        }
    }

    Update(value, speed) {
        this.views["to"].Update(value, speed);
    }

    AddNumberView() {
        this.views["to"] = new CounterNumberView(this.options);
        this.target.appendChild(this.views["to"].View);
    }

    AddSpecial(value, classname) {
        if (value !== "") {
            let item = document.createElement('div');
            item.className = "moview " + classname;
            item.innerHTML = value;
            this.target.appendChild(item);
        }
    }

    OnScrollUpdate(isVisible, direction) {
        // console.log("countto:: View:: OnScrollUpdate: isVisible, direction", isVisible, direction, this.target);
        if (isVisible && !this.started) {
            if (
                (direction == "up" && this.options.RestartUp == 1) || 
                (direction == "down" && this.options.RestartDown == 1) || 
                (direction == "" && this.options.StartInView == 1)
            ) {
                // console.log("start");
                this.ticker.Reset();
                this.options.SetCurrent(this.ticker.Current());
                this.started = false;
                this.finished = false;
                // console.log("countto:: View:: OnScrollUpdate: this.ticker.Current()", this.ticker.Current());
                this.Start();
            }
        } else if (!isVisible ) {
            // console.log("countto:: View:: OnScrollUpdate: this.options.RestartUp, this.options.RestartDown", this.options.RestartUp, this.options.RestartDown);
            if ((this.options.RestartDown > 0 || this.options.RestartUp > 0)) {
                if ( this.options.RestartDown == 1 && direction == "up" ) {
                    // console.log("reset for down restart");
                    this.ticker.Reset();
                    this.options.SetCurrent(this.ticker.Current());
                    this.Update(this.options.Current, this.ticker.TickTime);
                    this.started = false;
                    this.finished = false;
                    this.StopTicker();
                } else if ( this.options.RestartUp == 1 && direction == "down" ) {
                    // console.log("reset for up restart");
                    this.ticker.Reset();
                    this.options.SetCurrent(this.ticker.Current());
                    this.Update(this.options.Current, this.ticker.TickTime);
                    this.started = false;
                    this.finished = false;
                    this.StopTicker();
                }
            }
        }
    }

    StopTicker() {
        if (this.tickTimeout !== null) {
            clearTimeout(this.tickTimeout);
            this.tickTimeout = null;
        }
    }

    Start() {
        // console.log("Start::", this.id, this.ticker.TickTime);
        let scope = this;
        if (this.options.Timeout > 0) {
            let date = new Date();
            this.timeoutStartTime = date.getTime();
            this.tickTimeout = setTimeout(function () {
                EventManager.Throw("start", { id: scope.id, options: scope.options, target: scope.target, percent: scope.ticker.Percent });
                // scope.started = true;
                date = new Date();
                scope.timeoutStopTime = date.getTime();
                scope.tickTimeout = setTimeout(scope.Tick.bind(scope), 1, false);
            }.bind(this), this.options.Timeout);
        } else {
            EventManager.Throw("start", { id: this.id, options: this.options, target: this.target, percent: this.ticker.Percent });
            this.tickTimeout = setTimeout(this.Tick.bind(this), this.ticker.TickTime, false);
        }
        this.started = true;
    }

    Tick(stop) {
        this.options.SetCurrent(this.ticker.Next());
        EventManager.Throw("change", { id: this.id, options: this.options, target: this.target, percent: this.ticker.Percent });

        this.Update(this.options.Current, this.ticker.TickTime);
        if (this.ticker.Finished()) {
            clearTimeout(this.tickTimeout);
            this.tickTimeout = null;
            this.finished = true;
            EventManager.Throw("stop", { id: this.id, options: this.options, target: this.target, percent: this.ticker.Percent });
            return;
        }
        if (!stop && this.options.Debug !== 1) {
            this.tickTimeout = setTimeout(this.Tick.bind(this), this.ticker.TickTime, false);
        } else {
            this.started = false;
            this.finished = true;
        }
    }

}

module.exports = View;