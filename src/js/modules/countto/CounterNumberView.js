
import NumberView from '../tools/NumberView'; 

class CounterNumberView extends NumberView {

    constructor(options) {
        super(options, "moct", "moct");
        
    }
}

module.exports = CounterNumberView;