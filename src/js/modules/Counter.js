
import SingleCounter from './counter/SingleCounter';
import Options from './tools/Options';
import AttributReader from './tools/AttributReader';
import EventManager from './tools/Eventmanager';

class Counter {

    constructor() {
        this.attributeReader = new AttributReader();
        this.Init();
    }

    Init() {
        let counter = document.querySelectorAll('.mo-counter');
        alert("init counter");
        for ( let i = 0; i < counter.length; i++ ) {
            let options = this.attributeReader.Parse(counter[i], new Options());
            new SingleCounter(counter[i], options);
        }
    }

    AddEvent(type, callback) {
        EventManager.AddEvent(type, callback);
    }

    RemoveEvent(type, callback) {
        EventManager.RemoveEvent(type, callback);
    }
}

module.exports = Counter;