/**
 * @class   AttributReader
 * @description update the options object with the data found in target DOM elements dataset
 * @author  Morshiba
 * @license MIT
 */


class AttributReader {

    constructor() {

    }

    /**
     * @function    Parse
     * @description for any element in options object chack if there is a dataset value to update
     * @param {DOMObject} counter the counter html dom object
     * @param {Options} options the default options Object
     */
    Parse(counter, options) {
        for (let i in options.AsObject) {
            options[i] = this.GetData(i, options, counter);
        }
        return options;
    }

    /**
     * @function    GetData
     * @description test value and return new or default value
     * @param {string} name the attributes name
     * @param {Options} options the default options object
     * @param {DOMObject} counter the counter html object 
     */
    GetData(name, options, counter) {
        let setting;
        if (counter.dataset[name]) {
            switch (name) {
                case "targettime":
                case "starttime":
                    setting = this.getDataAsDate(counter.dataset[name]);
                    break;
                case "format":
                    setting = counter.dataset[name].split(".");
                    break;
                case "numbersize":
                    setting = this.getSizesAsObject(options.numbersize, counter.dataset[name]);
                    break;
                case "divider":
                    setting = this.getDivider(counter.dataset[name], options);
                    break;
                case "start":
                case "stop":
                case "restartdown":
                case "restartup":
                case "dezimal":
                case "time":
                case "speed":
                    setting = this.getNumber(counter.dataset[name]);
                    break;
                default:
                    setting = counter.dataset[name];
                    break;
            }
        } else {
            setting = options[name];
        }
        if (setting === "") {
            setting = options[name];
        }
        return setting;
    }

    getDataAsDate(setting) {
        let parts = setting.split(" ");
        let date = parts[0];
        let time = parts[1];
        date = date.split(".");
        time = time.split(":");
        return new Date(date[0], date[1] - 1, date[2], time[0], time[1], time[2]);
    }

    getSizesAsObject(before, setting) {
        let parts = setting.split(',');
        for (let i = 0; i < parts.length; i++) {
            let partParts = parts[i].split(':');
            before[partParts[0]] = Number(partParts[1]);
        }
        return before;
    }

    getDivider(string, options) {
        let response = options.divider;
        let parts = string.split(",");
        for (let i = 0; i < parts.length; i++) {
            let p = parts[i].split(":");
            response[p[0].trim()] = p[1];
        };
        return response;
    }

    getNumber(number) {
        if (String(number).indexOf(",") > -1) {
            return Number(String(number).split(",").join("."));
        } else {
            if ( String(number) === "" ) {
                return 0;
            }
            return Number(number);
        }
    }
}

module.exports = AttributReader; 