/**
 * @class   TimeParser
 * @author Morshiba
 * @description parse a time in miliseconds and save to an object with vlaues for any part of a time 
 *              containing full ( the full value of current element ) and rest ( the value when a higher calue will be displayd)
 *              example when we have 1 day and 1 hour then the value for the hour is full: 25, rest: 1 and the value for the day: full: 1, rest: 1
 * @useage      let parser = new TimeParser();
 *              parser.Parse(1000000000);
 *              console.log("Years: ", parser.Data["y"]); // {full: n, rest: n}
 * @license MIT
 */

class TimeParser {

    constructor() {
        this.time = 0;
        this.data = {
            y: { full: -1, rest: -1 },
            mo: { full: -1, rest: -1 },
            w: { full: -1, rest: -1 },
            d: { full: -1, rest: -1 },
            h: { full: -1, rest: -1 },
            m: { full: -1, rest: -1 },
            s: { full: -1, rest: -1 },
            ms: { full: -1, rest: -1 }
        };
    }

    /**
     * @function    Parse
     * @param {int} time    the time in miliseconds
     */
    Parse(time) {
        this.time = time;

        let UpdateIdAndGetNextValueFromTime = (v1, faktor, id) => {
            var v2 = Math.floor(v1 / faktor);
            this.data[id].full = v1;
            this.data[id].rest = Math.floor(v1 - (v2 * faktor));
            return v2;
        }

        if (time > 0) {
            var s = UpdateIdAndGetNextValueFromTime(time, 1000, 'ms');
            var m = UpdateIdAndGetNextValueFromTime(s, 60, 's');
            var h = UpdateIdAndGetNextValueFromTime(m, 60, 'm');
            var d = UpdateIdAndGetNextValueFromTime(h, 24, 'h');
            var w = UpdateIdAndGetNextValueFromTime(d, 7, 'd');
            var mo = UpdateIdAndGetNextValueFromTime(w, 4, 'w');
            var y = UpdateIdAndGetNextValueFromTime(mo, 12, 'mo');
            this.data["y"].full = y;
            this.data["y"].rest = y;
        }
    }

    
    /**
     * @function    Direction
     * @param {Options} options     the filled Options Object with the needed Informations
     * @returns     {Options}       the updated Options object   
     */
    Direction(options) {
        if ( options.Direction === "down" || options.Direction === "up") {
            return options;
        } else {
            let now = new Date();
            if ( now < options.TargetTime ) {
                options.Direction = "down";
            } else {
                options.Direction = "up";
            }
        }
        return options;
    }

    /**
     * @getter Data
     * @returns {object}    the data object with full and rest values 
     */
    get Data() {
        return this.data;
    }
    /**
     * @setter  Data
     * @param {object} data     an object representing the calculated data sheme    
     */
    set Data(data) {
        this.data = data;
    }

    /**
     * @getter Time
     * @returns {int}    the time value the calculated data values are calculated from 
     */
    get Time() {
        return this.time;
    }
}

module.exports = TimeParser;