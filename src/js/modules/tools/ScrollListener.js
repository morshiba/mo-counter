/**
 * @class   ScrollListener
 * @description fire events when an object comes into or goes out of the browser view
 * @author  Morshiba
 * @license MIT
 * @param   {DomObject} target  the element you want to watch for
 */

import ObjectCords from './ObjectCords';
import EventManager from './Eventmanager';

class ScrollListener {

    constructor(target) {
        this.target = target;
        this.lastScrollPosition = window.pageYOffset;
        this.scrollPosition = window.pageYOffset;
        this.windowHeight = this.GetWindowHeight();
        this.onChange = null;
        this.direction = "down";
        this.tick = false;
        this.limit = 1000/60;
        this.prevRatio = "";
        this.first = true;
        this.observer = null;
        this.hasObserver = false;
        this.windowListener = "";
        this.old_visible = false;
        this.isIE11 = !!window.MSInputMethodContext && !!document.documentMode;
        this.callback = null;
    }

    /**
     * @function Init
     * @description starts the watching process
     * @param {function} callback the callback to call if the visibility changes
     */
    Init(callback) {
        this.callback = callback;
        this.lastRect = this.target.getBoundingClientRect();
        this.ScrollWatcher();
    }

    /**
     * @function    ScrollWatcher
     * @description start the animationFrame request and check for changes in scrollstate and screen height
     */
    ScrollWatcher() {
        let scope = this;
        requestAnimationFrame(function watchScrollUpdates() {
            let currentScrollPosition = window.pageYOffset;
            let currentWindowHeight = scope.GetWindowHeight();
            if ( scope.scrollPosition != currentScrollPosition) {
                scope.UpdateDirection();
                scope.ScrollWatcherHandler();
            } else if ( currentWindowHeight != scope.windowHeight ) {
                if ( currentWindowHeight < scope.windowHeight ) {
                    scope.direction = "up";
                } else {
                    scope.direction = "down";
                }
                scope.ScrollWatcherHandler();
            }
            scope.scrollPosition = currentScrollPosition;
            scope.windowHeight = currentWindowHeight;
            requestAnimationFrame(watchScrollUpdates);
        });
        this.ScrollWatcherHandler();
    }

    /**
     * @function ScrollWatcherHandler
     * @description when the limit allows the visibility will be checked and the callback is called if it changed
     */
    ScrollWatcherHandler() {
        if (!this.tick) {
            this.tick = true;
            this.OnVisibilityChange(this.target, (el, isVisible) => {
                if (this.first && isVisible) {
                    this.direction = "";
                    this.first = false;
                }
                if (typeof this.callback == 'function') {
                    this.callback(isVisible, this.direction, el);
                }
                EventManager.Throw('visibilitychange', {
                    element: el,
                    direction: this.direction,
                    isVisible: isVisible
                });
            });
            setTimeout(function () {
                this.tick = false;
            }.bind(this), this.limit);
        }
    };

    /**
     * @function OnVisibilityChange
     * @description check if the visibility changed
     * @param {DOMElement} el 
     * @param {function} callback 
     */
    OnVisibilityChange(el, callback) {
        var visible = this.IsInView(el);
        if (visible != this.old_visible) {
            this.old_visible = visible;
            if (typeof callback == 'function') {
                callback(el, visible);
            }
            // console.log("SCROLL LISTENER:: ",{
            //     element: el,
            //     direction: this.direction,
            //     isVisible: visible
            // } );
        }
    }
    /**
     * @function    UpdateDirection
     * @description update direction string based on page Scroll position changes
     */
    UpdateDirection() {
        var pageY = window.pageYOffset;
        if (this.lastScrollPosition < pageY) {
            this.direction = "down";
        } else {
            this.direction = "up";
        }
        this.lastScrollPosition = pageY;
    }

    /**
     * @function    IsInView
     * @description test if the element is in the durrent view of the browser window
     * @param {DOMElement} el 
     * @returns {bool} true if is in view false otherwise
     */
    IsInView(el) {
        let scrollTop = this.GetScrollTop();
        let cords = new ObjectCords(el);
        let offsetHeight = scrollTop + this.GetWindowHeight();
        if ( (cords.top + cords.height < scrollTop) || (cords.top > offsetHeight) ) {
            return false;
        }
        return true;
    }

    /**
     * @function GetWindowHeight
     * @description get the current window height
     */
    GetWindowHeight() {
        var scrollTop = window.clientHeight || window.innerHeight;
        return scrollTop;
    }

    /**
     * @function    GetScrollTop
     * @description Get the current scrollTop position in document
     */
    GetScrollTop() {
        var body = document.body;
        var docEl = document.documentElement;
        var scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
        return scrollTop;
    }
}

module.exports = ScrollListener;