/**
 * @class   Uid
 * @description generate a 99% unique id string with 9 digits
 * @author  Morshiba
 * @license MIT
 */

class Uid {

    /**
     * @function Id
     * @description     get a unique string id with 9 digits
     */
    get Id() {
        return '_' + Math.random().toString(36).substr(2, 9);
    }
}

module.exports = new Uid();