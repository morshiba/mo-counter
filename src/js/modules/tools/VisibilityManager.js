/**
 * @class   VisibilityManager
 * @description watch the browser focus and call the given callback if the visibility change
 * @author  Morshiba
 * @license MIT
 */


class VisibilityManager {

    constructor() {
        this.callbacks = [];
        document.addEventListener('visibilitychange', this.visibilityChange.bind(this), false);
    }

    /**
     * @function visibilityChange
     * @description     event handler waiting for visibility change events
     *                  if visibility is hidden send new status to the registered callback
     */
    visibilityChange() {
        let status = false;
        if (document.visibilityState != "hidden") {
            status = true;
        }
        for ( let i = 0; i< this.callbacks.length; i++ ) {
            if ( this.callbacks[i] !== null ) {
                this.callbacks[i](status);
            }
        }
    }

    /**
     * @function Register
     * @param {function} callback
     * @description     add a callback to the callbackslist
     *                  any added callback will be called when visibility changed
     */
    Register(callback) {
        this.callbacks.push(callback);
    }
}

module.exports = VisibilityManager;