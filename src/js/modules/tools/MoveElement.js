/**
 * @class   MoveElement
 * @description move an element 
 * @author  Morshiba
 * @license MIT
 * @param   {DomObject} element  the element you want to move
 */


const Easings = {

    Linear() {
        return (timeFraction) => {
            return timeFraction;
        }
    }
}

class MoveElement {

    constructor(element) {
        this.element = element;
        this.startPosition = {
            top: parseInt(this.element.style.top),
            left: parseInt(this.element.style.left),
            opacity: this.element.style.opacity
        };
        this.calc = {
            top: 0,
            left: 0
        };
        this.direction = "up";
        this.callback = null;
        this.debug = true;
    }

    Run(duration, targets, callback) {
        this.callback = callback;
        this.getCalc(targets);
        this.animate({
            timing: function (timeFraction) {
                return timeFraction;
            },
            draw: this.draw.bind(this),
            duration: duration ? duration : 1000
        });
    }

    getCalc(targets) {
        if (this.startPosition.top < targets.top) {
            this.direction = "down";
            this.calc.top = targets.top - this.startPosition.top;
        } else {
            this.direction = "up";
            this.calc.top = this.startPosition.top - targets.top;
        }
        this.calc.opacity = targets.opacity;
    }

    draw(progress) {
        let newValue = 0;
        if (this.direction == "down") {
            newValue = this.startPosition.top + (this.calc.top * progress);
        } else if (this.direction == "up") {
            newValue = this.startPosition.top - (this.calc.top * progress);
        }
        this.element.style.top = newValue + 'px';
        if ( this.calc.opacity > 0 ) {
            this.element.style.opacity = progress;
        } else {
            this.element.style.opacity = 1 - progress;
        }
        if (progress * 100 >= 100) {
            this.callback(this.element);
        }
    }

    animate({ timing, draw, duration }) {

        let start = performance.now();

        requestAnimationFrame(function animate(time) {
            let timeFraction = (time - start) / duration;
            if (timeFraction > 1) timeFraction = 1;

            let progress = timing(timeFraction)

            draw(progress); // draw it

            if (timeFraction < 1) {
                requestAnimationFrame(animate);
            }

        });
    }
}

module.exports = MoveElement;