/**
 * @class   ObjectCords
 * @description get top and left position in document and width and height of DOM element
 * @author  Morshiba
 * @license MIT
 * @param   {DomObject} elem  the element you want to watch for
 */

class ObjectCords {

    constructor(elem) {
        var box = elem.getBoundingClientRect();
    
        var body = document.body;
        var docEl = document.documentElement;
    
        var scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
        var scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft;
    
        var clientTop = docEl.clientTop || body.clientTop || 0;
        var clientLeft = docEl.clientLeft || body.clientLeft || 0;
    
        var top  = box.top +  scrollTop - clientTop;
        var left = box.left + scrollLeft - clientLeft;
    
        return { top: Math.round(top), left: Math.round(left), width: box.width, height: box.height };
    }
}

module.exports = ObjectCords;