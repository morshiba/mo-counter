/**
 * @class   JSAnimatedNumber
 * @description create the full number view
 * @author  Morshiba
 * @license MIT
 * @param   {Options} options  the settings object
 * @param   {string} prefix  the info for date counter numbers
 * @param   {string} cssClass  the css class prefix 
 */

const JSNumber = require("./JSNumber");



class JSAnimatedNumber {

    constructor(options, prefix, cssClass) {
        this.tagType = 'div';
        this.prefix = prefix ? prefix : 'moct';
        this.options = options;
        this.numbers = [];
        this.tsndDots = {};
        this.size = 0;
        this.cssClass = cssClass ? cssClass : "moct";
    }


    Build() {
        let number, i, divider;
        let span = document.createElement(this.tagType );
        span.className = this.cssClass + "-n " + this.cssClass + "-n" + this.prefix;
        this.size = this.options.GetSize(this.prefix);

        for ( i = 0; i < this.size; i++ ) {
            number = new JSNumber(this.size - i, this.cssClass, this.options.AnimationDirection, this.options.Type, this.options.Tween);
            this.numbers.push(number);
            number.Position = (this.size - i);
            span.appendChild(number.View);
            let diff = (this.size-(i+1));

            if (this.options.Type != "counter") {
                if ( (diff % 3) === 0 && diff > 0) {
                    this.tsndDots[i] = document.createElement(this.tagType);
                    this.tsndDots[i].className = this.cssClass + '-d ' + this.cssClass + '-dtsnd';
                    this.tsndDots[i].innerHTML = this.options.DezimalDivider == "," ? "." : ",";
                    span.appendChild(this.tsndDots[i]);
                }
            }
        }

        if (this.options.Type != "counter") {
            if ( this.options.Dezimal > 0 ) {
                divider = document.createElement(this.tagType );
                divider.className = this.cssClass + '-d ' + this.cssClass + '-d' + this.prefix;
                divider.innerHTML = this.options.DezimalDivider;
                span.appendChild(divider);
                for ( let j = 0; j < this.options.Dezimal; j++ ) {
                    number = new JSNumber(-j, this.cssClass, this.options.AnimationDirection, this.options.Type, this.options.Tween);
                    number.Position = (-j);
                    span.appendChild(number.View);
                    this.numbers.push(number);
                }
            }
        }
        return span;
    }


    GetNumberSize(value) {
        parts = String(value).split('.');
        if ( parts[1].lenght < this.options.Dezimal ) {
            while(parts[1].lenght < this.options.Dezimal ) {
                parts[1] = parts[1] + "0";
            }
        }
        parts = parts.join('').split('');
        return parts.length;
    }

    Update(value, speed) {
        let i, j, parts, dezimal, number, part;
        dezimal = [];
        if ( this.options.Type == "goto" ) {
            dezimal = [];
            if ( this.options.Dezimal > 0 && value != Math.floor(value) ) {
                parts = String(value).split('.');
                dezimal = String(parts[1]).split('');
                parts = String(parts[0]).split('');
            } else if (value != Math.floor(value)) {
                parts = String(value).split('.');
                parts = String(parts[0]).split('');
            } else {
                if ( String(value).indexOf('.') > -1 ) {
                    value = value.split('.')[0];
                }
                parts = String(value).split('');
            }
            if ( dezimal.length < this.options.Dezimal ) {
                for (i=dezimal.length; i<this.options.Dezimal; i++) {
                    dezimal.push('0');
                }
            } else if ( this.options.Dezimal > 0 ) {
                dezimal = dezimal.splice(0, this.options.Dezimal);
            }

            for (i=parts.length; i<this.size; i++) {
                if( this.options.PrefixRest === 1 ) {
                    parts.unshift('0');
                } else {
                    parts.unshift('');
                }
            }
        } else if ( this.options.Type == "slots" ) {
            parts = value;

            if ( this.options.Dezimal > 0 ) {
                for ( i=0; i<this.options.Dezimal; i++ ) {
                    if ( parts.length > 0) {
                        part = parts.pop();
                        dezimal.unshift(part);
                    } else {
                        dezimal.push("0");
                    }
                }
            }
        } else if ( this.options.Type == "counter" ) {
            parts = String(value).split('');
            if ( parts.length < this.size ) {
                for ( i=parts.length; i<this.size; i++ ) {
                    parts.unshift("0");
                }
            }
        }
        for ( i=0; i<parts.length; i++ ) {
            if (this.numbers[i] ) {
                this.numbers[i].Animate(parts[i], speed);
            }
            if ( this.options.Type != "counter" ) {
                if ( this.tsndDots[i] && parts[i] === '' && !this.tsndDots[i].classList.contains('hidden') ) {
                    this.tsndDots[i].className = this.cssClass + '-d mo-d mo-dtsnd hidden';
                } else if ( this.tsndDots[i] && parts[i] !== '' && this.tsndDots[i].classList.contains('hidden') ) {
                    this.tsndDots[i].className = this.cssClass + '-d mo-d mo-dtsnd';
                }
            }
        }

        if ( this.options.Type != "counter" ) {
            if ( this.options.Dezimal > 0 ) {
                for ( j=0; j<dezimal.length; j++ ) {
                    number = this.numbers[i];
                    i++;
                    if ( number ) {
                        number.Animate(dezimal[j], speed);
                    }
                }
            }
        }
    }

    
}

module.exports = JSAnimatedNumber;