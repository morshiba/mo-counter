/**
 * @class   JSNumber
 * @description build and manage the DOM elements for the animated number view
 * @author  Morshiba
 * @license MIT
 * @param   {int} pos an integer with the position of the furrent number in view
 *                    will be added to th eclassname
 * @param  {string} prefix the prefix like ms ord d or m or mo for the datecount can be empty if not needed
 * @param  {string} direction up for moving up down for moving down leaf empty tween for tweening and empty for no transition effect
 * @param  {string} type  depricated no need for it anymore
 * @param  {int} tween 1 if the direction is empty and the animation will fade the numbers 0 no fade just change  
 */

import MoveElement from './MoveElement';

class JSNumber {

    constructor(pos, prefix, direction, type, tween) {
        this.type = type;
        this.time = 1000;
        this.prefix = prefix;
        this.tween = tween;
        this.pos = pos;
        this.tagType = 'div';
        this.next = null;
        this.startPosition = -20;
        this.endPosition = 0;
        this.value = null;
        this.direction = direction;
        this.hidden = null;
        this.outPositions = null;
        this.inPositions = null;
        this.Build();
        this.animations = {};

        this.debug = true;
        this.current = null;
    }

    set Speed(speed) {
        this.time = speed;
    }

    get Position() {
        return this.pos;
    }
    set Position(pos) {
        this.pos = pos;
    }

    Build() {
        let classname;
        
        this.view = document.createElement(this.tagType );
        classname = `${this.prefix}-nv ${this.prefix}-${this.pos}`;
        this.view.className = classname;
        this.view.dataset.value = 0;
        this.hidden = document.createElement(this.tagType );
        this.hidden.className = 'mo-nv-hidden';
        this.hidden.innerHTML = "0";
        this.view.appendChild(this.hidden);

        let current = document.createElement(this.tagType );
        current.className =`${this.prefix}sign ${this.prefix}-s active`;
        current.innerHTML = " ";
        current.style.top = this.endPosition + 'px';
        current.style.opacity = 1;
        this.view.appendChild(current);
        this.current = current;
    }

    GetMovePositions() {
        let offset = parseInt(window.getComputedStyle(this.hidden).fontSize, 10) * 0.8;
        this.outPositions = {
            up: -offset,
            down: offset,
            opacity: this.direction === "" ? (this.tween === 1 ? 0 : 1) : 0
        };
        this.inPositions = {
            up: offset,
            down: -offset,
            opacity: this.direction === "" ? (this.tween === 1 ? 0 : 1) : 0
        };
    }

    Animate(value, speed) {
        if (  this.value != value ) {
            if( this.outPositions === null ) {
                this.GetMovePositions();
            }
            this.value = value;
            this.Clean();
            this.current = this.view.getElementsByTagName(this.tagType)[1];
            let next = document.createElement('div');
            next.style.top =  this.inPositions[this.direction] + "px";
            next.style.opacity = ((this.tween === 1 || this.direction !== "" ) ? 0 : 1);
            next.className = `${this.prefix}sign ${this.prefix}-s ${this.prefix}-s-pos-${this.position}`;
            next.innerHTML = value;
            this.view.appendChild(next);
            
            this.Move(next, speed);
        }

    }

    Clean() {
        let items = this.view.getElementsByTagName(this.tagType);
        if ( items.length > 2 ) {
            while (items.length > 2 ) {
                items[1].parentNode.removeChild(items[1]);
            }
        }
    }

    Move(next, speed) {
        let current;

        if ( this.direction !== "" ) {
            current = new MoveElement(this.current);
            current.Run(speed*(this.tween === 1 ? 0.6 : 0.7), { top: this.outPositions[this.direction], opacity: this.outPositions.opacity}, function(target) {
                if ( target && target.parentNode ) {
                    target.parentNode.removeChild(target);
                }
            });
            
            let nextME = new MoveElement(next);
            nextME.Run(speed*0.8, { top: this.endPosition, opacity: 1}, function (target) {
                target.classList.add('active');
            });
        } else {
            if ( this.tween === 1 ) {
                current = new MoveElement(this.current);
                current.Run(speed*(this.tween === 1 ? 0.6 : 0.7), { top: this.endPosition, opacity: this.outPositions.opacity}, function(target) {
                    if ( target && target.parentNode ) {
                        target.parentNode.removeChild(target);
                    }
                });
                
                next = new MoveElement(next);
                next.Run(speed*0.8, { top: this.endPosition, opacity: 1}, function (target) {
                    target.classList.add('active');
                });
            } else {
                current = this.view.querySelector('.active');
                current.parentNode.removeChild(current);
                next.style.top = this.endPosition + 'px';
                next.style.opacity = 1;
                next.classList.add('active');
            }
        }
    }

    get View() {
        return this.view;
    }
}

module.exports = JSNumber;