/**
 * @class   DezimalObject
 * @description get a string or number value and retun an object with size informations
 * @author  Morshiba
 * @license MIT
 */


class DezimalObject {

    constructor(value) {
        this.value = value;
        this.data = {
            size: 0,
            dezimal: 0,
            parts: [0]
        };
        this.Parse();
    }

    Parse() {
        if ( String(this.value).indexOf(".") > -1 ) {
            this.data.parts = String(this.value).split(".");
            this.data.size = this.data.parts[0].length;
            this.data.dezimal = this.data.parts[1].length;
        } else {
            this.data.parts[0] = String(this.value);
            this.data.size = this.data.parts[0].length;
        }
    }

    Get() {
        return this.data;
    }
}

module.export = DezimalObject; 