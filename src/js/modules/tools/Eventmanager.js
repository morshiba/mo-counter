/**
 * @class   EventManager
 * @description register listener and send the informations to the CounterCallbacks object
 * @author  Morshiba
 * @license MIT
 */
import CounterCallback from '../CounterCallback';

class EventManager {
    constructor() {
        this.events = [];
    }

    /**
     * @function    AddEvent
     * @description add en listener to the list of listener
     * @param {string} type  the event type name
     * @param {function} callback a function to call when an event from given type occure
     */
    AddEvent(type, callback) {
        this.events.push({type: type, callback: callback});
    }

    /**
     * @function RemoveEvent
     * @description remove an event from the list
     * @param {string} type 
     * @param {function} callback 
     */
    RemoveEvent(type, callback) {
        let obj = {type: type, callback, callback};
        for ( let i = 0; i < this.events; i++ ) {
            if ( this.events[i] == obj) {
                this.events[i] = null;
                delete(this.events[i]);
            }
        }
    }

    /**
     * @function    Throw
     * @description send an event to all listeners by type
     * @param {string} type 
     * @param {object} data 
     */
    Throw(type, data) {
        if ( !data.type ) {
            data.type = type;
        }
        CounterCallback.GetCallback(type)(data);
    }
}

module.exports = new EventManager();