
import NumberSize from "../counter"

class Options {

    constructor() {
        this.prefixtime = 1;
        this.prefixrest = 0;
        this.removeempty = 1;
        this.prefix = "";
        this.format = 'y.mo.w.d.h.m.s.ms'.split('.');
        this.Tween = 1;

        this.divider = {
            y: "",
            mo: "",
            w: "",
            d: "",
            h: "",
            m: "",
            s: "",
            ms: ""
        };
        this.numbersize =  {
            ms: 3,
            s: 2,
            m: 2,
            h: 2,
            d: 2,
            w: 2,
            mo: 2,
            y: 2
        };
        this.debug = 0;
        this.killafter = 1;
        this.targettime = new Date();
        this.starttime = null;
        this.direction = "";
        this.animationDirection = "";
        this.start = 0;
        this.stop = 0;
        this.before = "";
        this.after = "";
        this.speed = 100;
        this.type = "goto";
        this.ease = "linear";
        this.startinview = 1;
        this.current = 0;
        this.restartdown = 0;
        this.restartup = 0;
        this.stepsize = 1;
        this.dezimal = 0;
        this.dezimaldivider = ",";
        this.time = 10000;
        this.timeout = 0;
        this.step = 1;
    }

    UpdateSize(timeData) {
        // console.log("timeData", timeData);
        let newSize = 0;
        // console.log("this.numbersize", this.numbersize);
        if ( this.format[0] == "y" ) {
            return;
        } else if ( this.format[0] == "mo" ) {
            newSize = String(timeData.mo.full).length;
            if ( newSize >= this.numbersize.mo ) {
                this.numbersize.mo = newSize;
            }
        } else if ( this.format[0] == "w" ) {
            newSize = String(timeData.w.full).length;
            if ( newSize >= this.numbersize.w ) {
                this.numbersize.w = newSize;
            }
        } else if ( this.format[0] == "d" ) {
            newSize = String(timeData.d.full).length;
            if ( newSize >= this.numbersize.d ) {
                this.numbersize.d = newSize;
            }
        } else if ( this.format[0] == "h" ) {
            newSize = String(timeData.h.full).length;
            if ( newSize >= this.numbersize.h ) {
                this.numbersize.h = newSize;
            }
        } else if ( this.format[0] == "m" ) {
            newSize = String(timeData.m.full).length;
            if ( newSize >= this.numbersize.m ) {
                this.numbersize.m = newSize;
            }
        } else if ( this.format[0] == "s" ) {
            newSize = String(timeData.s.full).length;
            if ( newSize >= this.numbersize.s ) {
                this.numbersize.s = newSize;
            }
        }
        // console.log("this.numbersize after ", this.numbersize);
    }

    /**
     * @getter AsObject
     * @returns {object} options all options as object
     */
    get AsObject() {
        return {
            prefixtime: this.PrefixTime,
            removeempty: this.RemoveEmpty,
            format: this.Format,
            divider: this.Divider,
            debug: this.Debug,
            killafter: this.KillAfter,
            targettime: this.TargetTime,
            direction: this.Direction,
            animationDirection: this.AnimationDirection,
            prefix: this.Prefix,
            numbersize: this.NumberSize,
            prefixrest: this.prefixrest,
            starttime: this.starttime,
            start: this.start,
            stop: this.stop,
            before: this.before,
            after: this.after,
            speed: this.speed,
            type: this.type,
            ease: this.ease,
            startinview: this.startinview,
            restartdown: this.restartdown,
            restartup: this.restartup,
            current: this.current,
            stepsize: this.stepsize,
            dezimal: this.dezimal,
            dezimaldivider: this.dezimaldivider,
            time: this.time,
            timeout: this.timeout,
            step: this.step,
            tween: this.tween
        };
    }

    Update(options) {
        let i;
        if ( options ) {
            if ( options.format ) {
                if( typeof(options.format) == "string" ) {
                    options.format = options.format.split('.');
                }
            }
            if ( options.divider ) {
                for ( i in options.divider ) {
                    this.divider[i] = options.divider[i];
                }
                options.divider = null;
                delete(options.divider);
            }
            for ( i in options ) {
                if ( this[i] ) {
                    this[i] = options[i];
                }
            }   
        }
    }

    GetDivider(value, id) {
        if ( this.divider[id] && this.divider[id] !== "" ) {
            if (  this.divider[id].indexOf('/') > -1 ) {
                if ( value > 1 || value === 0 ) {
                    return this.divider[id].split('/').join('');
                } else {
                    return this.divider[id].split('/')[0];
                }
            } else {
                return this.divider[id];
            }
        } 
        return "";
    }

    GetSpeedByType() {
        let result = this.speed;
        let diff = Math.abs(this.start -this.stop);
        let diffCurrent = Math.abs(this.current -this.start);
        switch(this.type) {
            case "ease-out":
                result = this.speed / 100 * ( 100 / diff * diffCurrent );
                break;
            case "ease-in":
                result = this.speed - (this.speed / 100 * ( 100 / diff * diffCurrent ) );
                break;
            default:
                result = this.speed;
                break;
        }
        return result;
    }


    GetSize(prefix) {
        let size, value, parts;
        if ( this.Type == "counter" ) {
            size = this.NumberSize[prefix];
        } else {
            value = this.Stop;
            if ( this.Stop < this.Start ) {
                value = this.Start;
            }
            if ( value != Math.floor(value)) {
                parts = String(value).split('.');
                size = String(parts[0]).length;
            } else {
                size = String(value).length;
            }
        }
        return size;
    }

    IsTimePart(current) {
        if (current == "ms" || current == "s" || current == "m" || current == "h") {
            return true;
        }
        return false;
    }

    get Tween() {
        return this.tween;
    }
    set Tween(tween) {
        this.tween = tween;
    }

    get Time() {
        return this.time;
    }
    set Time(time) {
        this.time = time;
    }

    get Step() {
        return this.step;
    }
    set Step(step) {
        this.step = step;
    }

    get Timeout() {
        return this.timeout;
    }
    set Timeout(timeout) {
        this.timeout = timeout;
    }

    get Dezimal() {
        return this.dezimal;
    }
    set Dezimal(dezimal) {
        this.dezimal = dezimal;
    }

    get DezimalDivider() {
        return this.dezimaldivider;
    }
    set DezimalDivider(dezimaldivider) {
        this.dezimaldivider = dezimaldivider;
    }

    get StepSize() {
        return this.stepsize;
    }
    set StepSize(stepsize) {
        this.stepsize = stepsize;
    }

    get Divider() {
        return this.divider;
    }
    set Divider(divider) {
        this.divider = divider;
    }

    get NumberSize() {
        return this.numbersize;
    }
    set NumberSize(numbersize) {
        this.numbersize = numbersize;
    }
    
    get Prefix() {
        return this.prefix;
    }
    set Prefix(prefix) {
        this.prefix = prefix;
    }
    
    get Start() {
        return this.start;
    }
    set Start(start) {
        this.start = start;
    }
    
    get Stop() {
        return this.stop;
    }
    set Stop(stop) {
        this.stop = stop;
    }
    
    get Current() {
        return this.current;
    }
    set Current(current) {
        this.current = this.current;
    }

    SetCurrent(current) {
        this.current = current;
    }
    
    get Before() {
        return this.before;
    }
    set Before(before) {
        this.before = before;
    }
    
    get After() {
        return this.after;
    }
    set After(after) {
        this.after = after;
    }
    
    get Speed() {
        return this.speed;
    }
    set Speed(speed) {
        this.speed = speed;
    }
    
    get Type() {
        return this.type;
    }
    set Type(type) {
        this.type = type;
    }
    
    get Ease() {
        return this.ease;
    }
    set Ease(ease) {
        this.ease = ease;
    }

    get Format() {
        return this.format;
    }
    set Format(format) {
        this.format = format;
    }

    get Direction() {
        return this.direction;
    }
    set Direction(direction) {
        this.direction = direction;
    }


    get AnimationDirection() {
        return this.animationDirection;
    }
    set AnimationDirection(animationDirection) {
        this.animationDirection = animationDirection;
    }

    get TargetTime() {
        return this.targettime;
    }
    set TargetTime(targettime) {
        this.targettime = targettime;
    }

    get StartTime() {
        return this.starttime;
    }
    set StartTime(targettime) {
        this.starttime = starttime;
    }

    get RemoveEmpty() {
        return Number(this.removeempty);
    }
    set RemoveEmpty(removeempty) {
        this.removeempty = removeempty;
    }

    get PrefixTime() {
        return Number(this.prefixtime);
    }
    set PrefixTime(prefixtime) {
        this.prefixtime = prefixtime;
    }

    get PrefixRest() {
        return Number(this.prefixrest);
    }
    set PrefixRest(prefixrest) {
        this.prefixrest = prefixrest;
    }

    get StartInView() {
        return Number(this.startinview);
    }
    set StartInView(startinview) {
        this.startinview = startinview;
    }

    get RestartDown() {
        return Number(this.restartdown);
    }
    set RestartDown(restartdown) {
        this.restartdown = restartdown;
    }

    get RestartUp() {
        return Number(this.restartup);
    }
    set RestartUp(restartup) {
        this.restartup = restartup;
    }



    get Debug() {
        return Number(this.debug);
    }
    set Debug(debug) {
        this.debug = debug;
    }


    get KillAfter() {
        return Number(this.killafter);
    }
    set KillAfter(killafter) {
        this.killafter = killafter;
    }
}

module.exports = Options;