/**
 * @class   NumberView
 * @description build the JS animated number view 
 * @author  Morshiba
 * @license MIT
 * @param   {Options} options  the options to calculate anything
 * @param   {string} prefix a string that will be added to the css classList 
 *                          the class is ${cssClass}-n${prefix}
 * @param   {string} cssClass like descriped in prefix description the string will be added before the -n
 */


import JSAnimatedNumber from './JSAnimatedNumber';

class NumberView {

    constructor(options, prefix, cssClass) {
        this.options = options;
        this.view = null;
        this.number = null;
        this.tagType = "div";
        this.prefix = prefix ? prefix : "moct";
        this.cssClass = cssClass ? cssClass : "moct";
        this.Init();
    }

    Init() {
        this.view = document.createElement(this.tagType);
        this.view.className = 'moview moview-to';

        this.view.appendChild(this.BuildNumbers());
    }

    BuildNumbers() {
        this.number = new JSAnimatedNumber(this.options, this.prefix, this.cssClass);
        let span = this.number.Build();
        return span;
    }

    Update(value, speed) {
        this.number.Update(value, speed);
    }

    get View() {
        return this.view;
    }
}

module.exports = NumberView;