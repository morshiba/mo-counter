


import SingleCounter from './modules/counter/SingleCounter';
import CounterCallback from './modules/CounterCallback';

class MoCounter {

    constructor() {
        this.Init(true);
    }

    Init(isAutoInit) {
        let counter = document.querySelectorAll('.mo-counter');
        for ( let i = 0; i < counter.length; i++ ) {
            if ( !counter[i].classList.contains('initialized') ) {
                if ( isAutoInit && ( typeof counter[i].dataset.autoinit == "undefined" || counter[i].dataset.autoinit == "true" ) ) {
                    new SingleCounter(counter[i]);
                    counter[i].classList.add("initialized");
                } else if ( !isAutoInit ) {
                    new SingleCounter(counter[i]);
                    counter[i].classList.add("initialized");
                }
            }
        }
    }

    Callbacks() {
        return CounterCallback;
    }
}

module.exports = MoCounter;
global.CounterCallbacks = CounterCallback;
global.MoCounter = new MoCounter();
