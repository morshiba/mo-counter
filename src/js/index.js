


import SingleCounter from './modules/counter/SingleCounter';
import CounterCallback from './modules/CounterCallback';
import CountTo from './modules/countto/CountTo';

class MoCounter {

    constructor() {
        this.Init(true);
    }

    checkAutoInit() {
        let counter = document.querySelectorAll('.mo-counter');
        for ( let i = 0; i < counter.length; i++ ) {
            if ( typeof counter[i].dataset.autoinit == "undefined" || counter[i].dataset.autoinit ) {

            }
        }
    }

    Init(isAutoInit) {
        let counter = document.querySelectorAll('.mo-counter');
        for ( let i = 0; i < counter.length; i++ ) {
            if ( !counter[i].classList.contains('initialized') ) {
                if ( isAutoInit && ( typeof counter[i].dataset.autoinit == "undefined" || counter[i].dataset.autoinit == "true" ) ) {
                    new SingleCounter(counter[i]);
                    counter[i].classList.add("initialized");
                } else if ( !isAutoInit ) {
                    new SingleCounter(counter[i]);
                    counter[i].classList.add("initialized");
                }
            }
        }
        counter = document.querySelectorAll('.mo-counter-to');
        for ( let i = 0; i < counter.length; i++ ) {
            if ( !counter[i].classList.contains('initialized') ) {
                if ( isAutoInit && ( typeof counter[i].dataset.autoinit == "undefined" || counter[i].dataset.autoinit == "true" ) ) {
                    new CountTo(counter[i]);
                    counter[i].classList.add("initialized");
                } else if ( !isAutoInit ) {
                    new CountTo(counter[i]);
                    counter[i].classList.add("initialized");
                }
            }
        }
    }

    Callbacks() {
        return CounterCallback;
    }
}

module.exports = MoCounter;
global.CounterCallbacks = CounterCallback;
global.MoCounter = new MoCounter();
