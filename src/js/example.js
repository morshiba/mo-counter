

import Uid from './modules/tools/Uid';

class ExampleBuilder {

    Build(target, options) {
        let view = this.GetView(options);
        this.AddParameters(view, options.parameter);
        target.appendChild(view);
    }

    GetView(options) {
        let view = document.createElement('div');
        view.id = Uid.Id;
        view.className = options.type + ' ' + options.animation + ' ' + options.design;
        return view;
    }

    AddParameters(view, parameter) {
        for ( let i in parameter ) {
            view.setAttribute("data-"+i, parameter[i]);
        }
    }
}

module.exports = ExampleBuilder;
global.ExampleBuilder = new ExampleBuilder();