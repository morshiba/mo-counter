
# Mo Counter - Callbacks

Das CounterCallbacks Object dient der Kommunikation mit dem Counter.

Die Events werden global abgegeben, das bedeutet das wenn mehr als 1 Counter in der Seite existieren, dann erreicht Counter 1 bei Complete die Funktion die hinzugefügt wurde, und Counter 2 auch.

Best practice ist es jedem Counter eine eindeutige id oder eine eindeutige Klasse zu geben, die dann im Callback geprüft werden kann um auf jeden Counter einzeln reagieren zu können.

## Die Events

Das CounterCallbacks Object verfügt über folgende events:

- *complete* -> Wird ausgeführt wenn der Counter die direction down hat und den Wert 0 erreicht oder unterschritten hat.

```javascript
CounterCallbacks.AddCallback("complete", function(data) {
    // In dem Beispiel wurde dem counter die Klasse calendar hinzugefügt.
    // über diese Klasse kann jetzt der Counter geprüft werden.
    if ( data.target.classList.contains('calendar') ) {
        // do something
    };
});
```

