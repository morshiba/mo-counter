# MoCounter - Der Html Code

Der Code zum einbetten des Counters in eine Webseite ist sehr einfach gehalten.

Im Grunde benötigt man nur ein Tag (span, div, p, ...) dem man die Klasse mo-counter und den data-targettime parameter gibt.
Wenn das Script geladen wurde sucht es automatisch nach Tags mit der Klasse mo-counter und startet diese.
Der data-targettime Parameter ist dabei wichtig, da dieser als Grundberechnugswert genutzt wird. Ohne diesen funktioniert es nicht.

## Der Aufbau

```html
<div class="mo-counter" data-targettime="2021.1.28 21:45"></div>
```


### Die Steuerung der Zahlenanimation

Um die Laufrichtung der Zahlen im Counter zu bestimmen kann man folgende Klassen benutzen:

- *mo-up* -> Die Zahlen bewegen sich von unten nach oben
- *mo-down* -> Die Zahlen bewegen sich nach unten
- Wenn keine der beiden Klassen benutzt werden, dann läuft der Counter ohne Animation der Zahlen. Die Zahlen wechseln dann einfach.

```html
<!-- counter mit hoch laufenden Zahlen -->
<div class="mo-counter mo-up" data-targettime="2021.1.28 21:45"></div>

<!-- counter mit runter laufenden Zahlen -->
<div class="mo-counter mo-down" data-targettime="2021.1.28 21:45"></div>
```

### Responsivess Design

Um einen Umbruch der Darsstellung bei Screens kleiner 768px zu erreichen kann man die Klasse responsive mit hinzufügen

- *responsive* -> bewirkt das der Counter bis 767px alle Werte untereinander ausgibt.

```html
<!-- responsiver counter mit hoch laufenden Zahlen -->
<div class="mo-counter mo-up responsive" data-targettime="2021.1.28 21:45"></div>
```


## Die Attribute zur Steuerung des Counters

Der Counter hat ein paar einstellungen, die von aussen über das Html Element per data Attribut übergeben werden können.
Die parameter werden als ```data-parametername``` übergeben.

!ACHTUNG! Der einzige Parameter der auf jedem Fall vorhanden sein muss ist der ```targettime``` Parameter

Die Integration mit alles Parametern sieht so aus:

```html
    <div class="mo-counter mo-up responsive" 
        data-targettime="2051.2.31 10:44:00" 
        data-direction="down"
        data-format="y.mo.w.d.h.m.s.ms"
        data-removeempty="1"
        data-divider="y: Year/s&nbsp;,mo: Month ,w: Week/s ,d: Day/s"
        data-debug="1"
        data-killafter="5500"
        data-numbersize="w:2,d:3"
        data-prefixtime="1" 
        data-prefixrest="1"
        data-autoinit="false">
    </div>
```


### Die Attribute im Detail

## autoinit

Kann auf false gesetzt werden, damit der Counter oder der Counto nicht automatish initiiert wird.
Wenn autoinit="false" übergeben wird, muss händisch der Counter initiiert werden.

Ist der wert nicht gegeben oder = true, initiiert sich das Script eigenständig.

## targettime

```data-targettime="YYYY.MM.DD HH:MM:SS"```
Beispiel: ```data-targettime="2021.1.28 21:45:00"```

#### Aufbau des Zeittextes

Jahr.(Monat-1).Tag Stunden:Minuten:Sekunden

!Achtung! Der Monat muss mit -1 übergeben werden, da das Date Object in Javascript für den Januar mit der 0 beginnt.



## prefixtime

```data-prefixtime="1|0"```

Der Wert prefixtime bestimmt ob vor den Stunden, Minuten, Sekunden und Millisekunden die 0 bei Zahlen kleiner 10 dargestellt wird oder nicht.

1 = (default) Null wird vorangestellt

0 = Keine = voranstellen

## prefixrest

```data-prefixrest="1|0"```

Der Wert prefixrest bestimmt wie der Wert prefixtime ob eine 0 vorangestellt werden soll oder nicht. Diesmal jedoch für Tage, Wochen Monate und Jahre

## prefix

```data-prefix="Prefix Text"```

Der Prefix Wert wird, wenn er übergeben wurde als erstes Element in den Counter gerendert. 

*Beislpiel:* 
Prefix Text 1 Tag 02:01:54

## divider

Mit den Dividern können Werte die zwischen den Datums Informationen stehen können übergeben werden.

Default Werte:
```javascript
{
    y: " Jahr/e ",
    mo: " Monat/e ",
    w: " Woche/n ",
    d: " Tag/e ",
    h: ":",
    m: ":",
    s: ":",
    ms: ""
}
```

Überschreiben kann man die Werte so:
```data-divider="y:Year/s,mo:Month,w:Week/s,d:Day/s"```

Wenn man die Abstände nicht durch das eigene Desig bestimmen möchte sondern durch Lehrzeichen im divider Wert, dann müssen die Leerzeichen als &nbsp; übergeben werden. Es können auch nur einzelne Werte geändert werden
```data-divider="y:&nbsp;Year/s&nbsp;"```


## format

Mit dem Parameter format kann man bestimmen welche Werte in welcher Reihenfolgge ausgegeben werden sollen.

Default Werte:
```javascript
'y.mo.w.d.h.m.s.ms'
```

y = Jahr
mo = Monat
w = Woche
d = Tag
h = Stunde
m = Minute
s = Sekunde
ms = Milisekunde

```data-format="d.h.m"```
Es werden nur die Tage, die Stunden und die Minuten ausgegeben 

## numbersize

Mit diesem Parameter kann man bestimmen wieviele Stellen ein Wert haben soll.


Default Werte:
```javascript
{
   ms: 3,
    s: 2,
    m: 2,
    h: 2,
    d: 2,
    w: 1,
    mo: 2,
    y: 2
}
```

Überschreiben kann man die Werte wie folgt:

```data-numbersize="d:1,h:3"```

## direction

Um festzulefgen ob die Zeit Seid oder bis dargestellt werden soll kann man diesen Parameter eingeben. Wird er nicht angegeben ermittelt der counter selber was passieren soll.

Achtung wenn man die CounterCallback Funktionen nutzen möchte, dann muss man diesen Wert angeben um sicher zu sein das ein complete Eent gefeuert wird.

down = bedeutet das die Zeit bis dargestellt 

up = Die Zeit seit wird dargestellt


## removeempty

Dieser Parameter legt fest ob Leere Werte bei den Werten Jahr, Monat, Woche oder Tage gelöscht werden sollen 

1 = löschen

0 = lassen

# Spezial Parameter die für das debuggen praktisch sind

## debug

1 = setze einen timeout der das Script wieder stopt.

0 = deaktivert den debug mode

## killafter

Die Zeit in Millisekunden nach dem der Counter gestoppt werden soll wenn der debug mode = 1 ist.

