# MoCounter

## Simpler Zeit Counter

Mit dem MoCounter ist man in der lage ohne viel Aufwand einen Counter in seiner Webseite einzubinden.
Der Counter kann dabei entweder die Zeit die seid einem bestimmten ZEitpunkt vergangen ist darstellen, oder die Zeit bis zu einem Event in der Zukunft.

Bei bedarf ist es möglich einen Callback für den complete Event hinzuzufügen, um auf das ablaufen einer Zeit zu reagieren.

### Installation

1. Clone git repository
2. Open Project in cmd / terminal 
3. run ```npm install``` in projects root folder

### Build Solution

run ```grunt build``` to build the browserifyed version

run ```grunt watch``` to start building on demand while working

Nach dem man das Projekt mit ```grunt build``` gebaut hat, findet man die Scripte und Styles im Ordner build.
Eine Beispiel index.html ist im build Ordner vorhanden. 

# HowTo use

## Javascript

### Integration 

Füge das Script am ende deiner Webseite gleich über dem schließenden body Tag ein.
Nachdem das Script geladen werde sucht es automatisch in der Seite nach der Klasse mo-counter und initiiert die Counter.

```<script type="text/javascript" src="./js/mocounter.min.js"></script>```

Füge das Css am besten in den head bereich deienr Seite ein.

```<link rel="stylesheet" href="css/style.min.css">```

Natürlich könnt Ihr das Script und die Stylesheet Datei überall ablegen wo Ihr möchtet und müsst dann nur den Pfad anpassen.


### Html

Um einen Counter in seiner Webseite zu platzieren muss man nur ein DOM Element hinzufügen und diesem die css Klasse ```mo-counter``` und mindestens einen Wert für die Zielzeit angeben.

```html
<div class="mo-counter" data-targettime="2021.1.28 21:45"></div>
```
In diesem Beispiel wird der Counter ermitteln ob die targettime in der Zukunft liegt, in dem Fall wird ein Counter der runter zählt erstellt, oder ob es in der Vergangenheit liegt, dann zählt der Counter hoch.


[Mehr Informationen zum Html Code und den Attributen die benutzt werden können hier](./docs/ReadmeHtml.md)

# Das Aussehen

Der Counter wurde bewusst ohne optische aufhübschung gebaut.
Die styles die hinzugefügt wurden dienen nur der Animation und der Positionierung.

Um zu sehen wie man mit einem extra css Style die verschiedensten Optiken zaubern kann, 
schau im scss/example Ordner nach was es dort so gibt. In der index.html die im build Ordner erstellt wird kann man sich die Beispiele ansehen. Einfach im Browser öffnen.

Fühlt euch frei im scss/counter Ordner Anpassungen vorzunehmen damit es in Euer Projekt passt.
Zum Beispiel ist es sinnvoll wenn man nur eine up Animation haben möchte, die down Animation Styles zu löschen und dann das css neu zu bauen. Das macht die css Datei kleiner :)


# Callbacks

Um auf das ablaufen eines Timers reagieren zu können gibt es das CounterCallbacks Object. 

[Mehr Informationen zum CounterCallbacks Object hier](./docs/ReadmeCallbacks.md)